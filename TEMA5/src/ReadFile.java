import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Collection;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class ReadFile {


    public static void main(String args[]) {
        String fileName = "D://java programe//TEMA5//Activity.txt";
        List<String> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            list = stream
                    .map(String::toString)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<MonitoredData> monitoredDataList = new ArrayList<>();
        for(String listElement: list){
            String[] monitoredDataObject = listElement.split("\\t");
            
            monitoredDataList.add(new MonitoredData(monitoredDataObject[0],monitoredDataObject[2],monitoredDataObject[4]));
            System.out.println(monitoredDataObject[0]+ " " +monitoredDataObject[2] + " " +monitoredDataObject[4]);
        }
        
        
        
        String fileName1 = "out.txt";
        try{
        	PrintWriter outputStream = new PrintWriter(fileName1);
        
        	////////////////// numarul activitatilor puse intr-un fisier////////////////////
        	   Map<String,Long> resultingMap = monitoredDataList.stream()
                       .collect(groupingBy(MonitoredData::getActivity,counting()));
        	   resultingMap.entrySet().forEach(outputStream::print);
        	 
        	   //////////////////////generarea structurii de date pentru a afisa fiecare zi diferita////////////////////////////////////
        	  
        	   Map<String,Map<String,Long>> resultingMap2 = monitoredDataList.stream()
                       .collect(Collectors.groupingBy(MonitoredData::getStartTime,groupingBy(MonitoredData::getActivity,counting())));
        	   resultingMap2.entrySet().forEach(outputStream::println);
        	   outputStream.close();
        	   
        	   

        	
        }
        catch (FileNotFoundException e){
        e.printStackTrace();
        }
        
        
        
        
        
        /////////////////// FIRST ONE //////////////////////////
        ////////////////// .map =>>> fac split la starting time ca sa am un array de 2 elemente in care primul element ii startingTime DATE, startingTime TIME
        long distinctDays = monitoredDataList.stream()
                                             .map(monitoredData -> { String[] partitionedStartingTime = monitoredData.getStartTime().split(" ");
                                                                     return partitionedStartingTime[0];
                                                                    })
                                             .distinct()
                                             .count();
        System.out.println(distinctDays);





        /////////////////// SECOND ONE //////////////////////////
        Map<String,Long> resultingMap = monitoredDataList.stream()
                                                         .collect(groupingBy(MonitoredData::getActivity,counting()));
        resultingMap.entrySet().forEach(System.out::println);



        //////////////////////////// THIRD ONE////////////////////////////
        Map<String,Map<String,Long>> resultingMap2 = monitoredDataList.stream()
                                                                      .collect(Collectors.groupingBy(MonitoredData::getStartTime,groupingBy(MonitoredData::getActivity,counting())));
        resultingMap2.entrySet().forEach(System.out::println);
    }
   
		
		   
		
		
		
	}
	
